
CREATE TABLE account_category(
    id serial PRIMARY KEY,
    name VARCHAR NOT NULL UNIQUE,
    description VARCHAR,
    dateupdated TIMESTAMPTZ);

CREATE TABLE account_types(
    id serial PRIMARY KEY,
    name VARCHAR NOT NULL,
    category_id int NOT NULL,
    description VARCHAR,
    dateupdated TIMESTAMPTZ,
    FOREIGN KEY(category_id) REFERENCES account_category(id)
    );

CREATE TABLE accounts(
    id serial PRIMARY KEY,
    name VARCHAR NOT NULL,
    account_type_id int NOT NULL,
    parent_account_id VARCHAR,
    dateupdated TIMESTAMPTZ,
    FOREIGN KEY(account_type_id) REFERENCES account_types(id),
    FOREIGN KEY(parent_account_id) REFERENCES accounts(id)  
    );

CREATE TABLE header_types(
    id serial PRIMARY KEY,
    name VARCHAR NOT NULL,
    description VARCHAR,
    dateupdated TIMESTAMPTZ
    );

CREATE TABLE headers(
    id serial PRIMARY KEY,
	account_id integer,
    header_type_id integer,
    headernumber integer,
    headerdate TIMESTAMPTZ,
    dateupdated TIMESTAMPTZ,
    FOREIGN KEY(header_type_id) REFERENCES header_types(id),
	FOREIGN KEY(account_id) REFERENCES accounts(id)
    );
    
CREATE TABLE transactions
(
    id serial PRIMARY KEY,
    transaction_number integer NOT NULL DEFAULT 1,
    user_id integer NOT NULL,
    user_name character varying NOT NULL,
    department_id integer NOT NULL,
    department_name character varying NOT NULL,
    line_item_no integer,
    name character varying NOT NULL,
    account_id integer NOT NULL,
    quantity integer,
    price double precision,
    amount double precision,
    headers_id integer,
    dateupdated timestamp with time zone,
    product_id integer,
    FOREIGN KEY (account_id) REFERENCES products(id),
    FOREIGN KEY (headers_id) REFERENCES headers (id),
    FOREIGN KEY (product_id) REFERENCES products (id)
)

    
CREATE TABLE contacts(
    id serial PRIMARY KEY,
    supplementalId VARCHAR,
    iscompany boolean,  
    companyName VARCHAR,
    firstname VARCHAR NOT NULL,
    middlename VARCHAR,
    lastname VARCHAR NOT NULL,
    cellphone VARCHAR,
    homephone VARCHAR,
    faxnumber VARCHAR,
    officephone VARCHAR,
    emailaddress VARCHAR,
    streetaddress VARCHAR NOT NULL,
    city VARCHAR NOT NULL,
    state VARCHAR NOT NULL,
    country VARCHAR NOT NULL,
    postalcode VARCHAR NOT NULL,
    landmark VARCHAR,
    additionalcomments VARCHAR,
    idtype VARCHAR NOT NULL,
    idNumber VARCHAR NOT NULL,
    designation VARCHAR NOT NULL,
    dateupdated TIMESTAMPTZ
);

CREATE SEQUENCE sequencer START 1;

CREATE SEQUENCE header_sequencer START 1;

ALTER TABLE transactions ADD COLUMN transaction_number int NOT NULL DEFAULT 1



CREATE TABLE transaction_relations(
    id serial UNIQUE,
    user_id int NOT NULL,
    department_id int NOT NULL,
    transaction_id int not null,
    contact_id int not null,
    header_id int not null,
    dateupdated TIMESTAMPTZ,
    PRIMARY KEY (transaction_id,contact_id,header_id),
    FOREIGN KEY(contact_id) REFERENCES contacts(id),
    FOREIGN KEY(header_id) REFERENCES headers(id)
    );
